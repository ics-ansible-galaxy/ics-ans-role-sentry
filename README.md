ics-ans-role-sentry
===================

Ansible role to install Sentry.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
sentry_network: sentry-network
sentry_postgres_volume_name: sentry-postgres
sentry_filestore_volume_name: sentry-filestore
sentry_container_name: sentry
sentry_image: "registry.esss.lu.se/ics-docker/sentry:{{ sentry_image_tag }}"
sentry_image_tag: latest
sentry_secret_key: "00000000000000000000000000000000000000000000000000"
sentry_frontend_rule: "Host:{{ ansible_fqdn }}"
sentry_worker_container_name: sentry-worker
sentry_cron_container_name: sentry-cron
sentry_upgrade_container_name: sentry-upgrade
sentry_redis_container_name: sentry-redis
sentry_redis_image: redis:3.2-alpine
sentry_postgres_container_name: sentry-postgres
sentry_postgres_image: postgres:9.5
sentry_postgres_user: sentry
sentry_postgres_password: sentry
sentry_ldap_server_uri: ""
sentry_ldap_starttls: "true"
sentry_ldap_bind_dn: ""
sentry_ldap_bind_password: ""
sentry_email_host: localhost
sentry_email_port: "587"
sentry_email_user: username
sentry_email_password: password
sentry_email_use_tls: "true"
sentry_server_email: root@localhost
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-sentry
```

License
-------

BSD 2-clause
