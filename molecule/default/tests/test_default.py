import json
import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_containers(host):
    with host.sudo():
        cmd = host.run('docker ps')
    assert cmd.rc == 0
    names = sorted([line.split()[-1] for line in cmd.stdout.strip().split('\n')[1:]])
    assert names == [u'sentry', u'sentry-cron', u'sentry-postgres', u'sentry-redis', u'sentry-worker', u'traefik_proxy']


def test_api(host):
    cmd = host.run("curl --insecure --location --fail https://ics-ans-role-sentry-default/api/0/")
    assert cmd.rc == 0
    assert json.loads(cmd.stdout.strip())['version'] == "0"


def test_web(host):
    cmd = host.run("curl --insecure --location --fail https://ics-ans-role-sentry-default/")
    assert cmd.rc == 0
    assert "<title>Login | Sentry</title>" in cmd.stdout
